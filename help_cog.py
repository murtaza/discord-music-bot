import discord
from discord.ext import commands
from discord import app_commands

class help_cog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
        self.help_message = """
```
General commands
/help - Displays all the available commands
/p <keywords> - Finds the song on youtube and plays it in the voice channel. [Resumes if paused]
/q - Displays the current music queue
/skip - Skips the current song being played
/clear - Stops music and clears the queue
/leave - Disconnects the music bot
/pause - Pauses the current song, resumes if already paused
/resume - resumes current song   
```
"""
        self.text_channel_text = []
    '''
    @commands.Cog.listener()
    async def on_ready(self):
        for guild in self.bot.guilds:
            for channel in guild.text_channels:
                self.text_channel_text.append(channel)
        
        await self.send_to_all(self.help_message)
        
    async def send_to_all(self, msg):
        for text_channel in self.text_channel_text:
            await text_channel.send(msg)
    '''
    @app_commands.command(name="help", description="Displays all the available commands")
    async def help(self, interaction: discord.Interaction):
        await interaction.response.send_message(self.help_message)