import discord 
from discord.ext import commands
from discord import app_commands
import asyncio

from help_cog import help_cog
from music_cog import music_cog

Intents = discord.Intents.all()
Intents.message_content = True
Intents.voice_states = True

bot = commands.Bot(command_prefix=".", intents=Intents, application_id='1149884843765809273')

async def setup(bot):
    bot.remove_command("help")
    await bot.add_cog(help_cog(bot))
    await bot.add_cog(music_cog(bot))

coro = setup(bot)
asyncio.run(coro)

@bot.event
async def on_ready():
    print("bot is up and running")
    try:
        synced = await bot.tree.sync()
        print(f"Synced {len(synced)} command(s)")
    except Exception as e:
        print(e)

bot.run("MTE0OTg4NDg0Mzc2NTgwOTI3Mw.G9roqW.m3oejvH4XP3jgh43hfyepr3UkHo3PvtlhkQqgc")

    