from typing import Any
import discord 
from discord.ext import commands
from youtube_dl import YoutubeDL
from discord import app_commands


class music_cog(commands.Cog):
    def __init__(self, bot):
        print("bot created")
        self.bot = bot
        
        self.is_playing = False
        self.is_paused = False
        self.now_playing = "Nothing"
        
        self.music_queue = []
        self.YDL_OPTIONS = {'format': 'bestaudio', 'noplaylist': 'True'}
        self.FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn -sn -b:a 96k'}
        self.SPOT_OPTIONS = {}
        
        self.vc = None
    
    def search_yt(self, item):
        with YoutubeDL(self.YDL_OPTIONS) as ydl:
            try:
                info = ydl.extract_info("ytsearch:%s" % item, download=False)['entries'][0]
            except Exception:
                return False
        return {'source': info['formats'][0]['url'], 'title': info['title']}
    
    def play_next(self):
        if len(self.music_queue) > 0:
            self.is_playing = True
            
            m_url = self.music_queue[0][0]['source']
            
            self.music_queue.pop(0)
            
            self.vc.play(discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS), after=lambda e: self.play_next())
        else:
            self.is_playing = False
            
            
    async def play_music(self, interaction: discord.Interaction):
        if len(self.music_queue) > 0:
            self.is_playing = True
            
            m_url = self.music_queue[0][0]['source']
            self.now_playing = self.music_queue[0][0]['title']
            if self.vc == None or not self.vc.is_connected():
                self.vc = await self.music_queue[0][1].connect()
                if self.vc == None:
                    await interaction.response.send_message("Could not connect to voice channel")
                    return
            else:
                await self.vc.move_to(self.music_queue[0][1])
        
            self.music_queue.pop(0)
            self.vc.play(discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS), after=lambda e: self.play_next())
            
        else:
            self.is_playing = False
            self.now_playing = "Nothing"
    
    
    @commands.command()
    async def sync(self, ctx) -> None:
        fmt = await ctx.bot.tree.sync(guild=ctx.guild)
        
        await ctx.send(f'Synced {len(fmt)}')
    
    @app_commands.command(name="play", description="Play the selected song from YouTube")
    async def play(self, interaction: discord.Interaction, query: str):
        query = "".join(query)
        voice_channel = interaction.user.voice.channel
        if voice_channel is None:
            await interaction.response.send_message('Join a channel to play music')
        elif self.is_paused:
            self.vc.resume()
        else:
            
            song = self.search_yt(query)
            
            if type(song) == type(True):
                await interaction.response.send_message("Could not download the song. Incorrect format, try a different keyword")
            else:
                await interaction.response.send_message("Song added to queue: " + song["title"])
                self.music_queue.append([song, voice_channel])
                
                if self.is_playing == False:
                    await self.play_music(interaction)
                    
    @app_commands.command(name="pause", description="pauses the song")
    async def pause(self, interaction: discord.Interaction):
        if self.is_playing:
            self.is_playing = False
            self.is_paused = True
            self.vc.pause()
            await interaction.response.send_message("⏸️ Paused " + self.now_playing)
        elif self.is_paused:
            self.vc.resume()
            await interaction.response.send_message("▶️ Resuming " + self.now_playing)
       
    @app_commands.command(name="resume", description="resumes the song")
    async def resume(self, interaction: discord.Interaction):
        if self.is_paused:
            self.is_playing = True
            self.is_paused = False
            self.vc.resume()
            await interaction.response.send_message("▶️ Resuming " + self.now_playing)
            
    @app_commands.command(name="skip", description="skips the song")
    async def skip(self, interaction: discord.Interaction):
        if self.vc != None and self.vc:
            self.vc.stop()
            await interaction.response.send_message("⏩ Skipped " + self.now_playing)
            await self.play_music(interaction)
            
    @app_commands.command(name="queue", description="shows the current queue")
    async def queue(self, interaction: discord.Interaction):
        retval = "Now Playing 🎶: " + self.now_playing + "\n"

        for i in range(0, len(self.music_queue)):
            retval += "> "+ str(i) + ". " + self.music_queue[i][0]['title'] + '\n'
            
        if retval != "":
            await interaction.response.send_message(retval)
        else:
            await interaction.response.send_message("No music in the queue.")
            
    @app_commands.command(name="clear", description="clears the current queue")
    async def clear(self, interaction: discord.Interaction):
        if self.vc != None and self.is_playing:
            self.vc.stop()
        self.music_queue = []
        await interaction.response.send_message("Music queue cleared")
    
    @app_commands.command(name="leave", description="clears the current queue")
    async def leave(self, interaction: discord.Interaction):
        self.is_playing = False
        self.is_paused = False
        await interaction.response.send_message("Disconnected :wave_tone5:")
        await self.vc.disconnect()
        