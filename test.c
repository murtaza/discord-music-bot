#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

bool isContainedInArr (int * prevNumbers, int num, int size) {
    for (int i = 0; i < size; i++) {
        printf(prevNumbers[i]);
        if (prevNumbers[i] == num) {
            return true;
        }
    }
    return false;
}

int main (void) {
    int * prevNumbers = (int*) calloc(0, sizeof(int));

    if (prevNumbers == NULL) {
        printf("Memory not allocated.\n");
        exit(0);
    }

    int size = 0; // If I don't start at 1 and use size - 1 realloc fails after the 7th time size is increased and it is called
    int inputNum = -1;
    for (int i = 0; i < 20; i++) {
        printf("%s", "Enter a number: ");
        scanf("%d", &inputNum);
        if (!isContainedInArr(&prevNumbers, inputNum, size)) {
            prevNumbers[size] = inputNum;
            size++;
            
            printf("Size is %d\n", size); // Test code
            printf("Size is %d\n", size * sizeof(int)); // Test code

            prevNumbers = realloc(prevNumbers, size * sizeof(int));
            printf("%d\n", inputNum);
        } else {
            continue;
        }
    }
}
